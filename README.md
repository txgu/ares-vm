# README

A simplified Java interpreter that can be used as the sandbox for testing synthesized error handler.

This project aims to replace the JPF component in ares.
Besides, we plan to implement more synthesis and ranking strategies in the future based on this Java interpreter.
