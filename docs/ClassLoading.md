# The Design of Class Loading

The flow of `resolveClassInfo` on a given `ClassLoaderInfo` object `this`.

1. Use delegate to find the `java.lang.Class` object `hostClass`.
2. Use `hostClass` to find the host class loader `java.lang.ClassLoader` object `hostLoader`.
3. Use `hostLoader` to find the `ClassLoaderInfo` object `loaderInfo`.
4. If the `loaderInfo` is not `this`, invoke `resolveClassInfo` on the `loaderInfo`.
2. Otherwise, use delegate to find the class file
    * This will recursively find the class file.
3. Define the class file with the `this`
