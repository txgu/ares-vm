package org.javelus.ares.vm;

import java.util.HashMap;
import java.util.Map;

public class InstanceElementInfo extends ElementInfo implements Cloneable {

    public InstanceElementInfo(ClassInfo classInfo) {
        super(classInfo);
    }

    private Map<FieldInfo, Value> fieldsToValue = new HashMap<FieldInfo, Value>();

    public void setField(FieldInfo field, Value value) {
        if (value == null) {
            throw new NullPointerException();
        }
        fieldsToValue.put(field, value);
    }

    protected Value getDefaultValue(FieldInfo field) {
        return Value.getDefaultValue(field.getDescriptor());
    }
    
    public Value getField(FieldInfo field) {
        Value value = fieldsToValue.get(field);
        if (value == null) {
            value = getDefaultValue(field);
            fieldsToValue.put(field, value);
        }
        return value;
    }

    public InstanceElementInfo clone() {
        return null;
    }
}
