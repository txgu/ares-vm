package org.javelus.ares.vm;

import static com.sun.xml.internal.ws.org.objectweb.asm.Type.BOOLEAN;
import static com.sun.xml.internal.ws.org.objectweb.asm.Type.BYTE;
import static com.sun.xml.internal.ws.org.objectweb.asm.Type.CHAR;
import static com.sun.xml.internal.ws.org.objectweb.asm.Type.DOUBLE;
import static com.sun.xml.internal.ws.org.objectweb.asm.Type.FLOAT;
import static com.sun.xml.internal.ws.org.objectweb.asm.Type.INT;
import static com.sun.xml.internal.ws.org.objectweb.asm.Type.LONG;
import static com.sun.xml.internal.ws.org.objectweb.asm.Type.SHORT;
import static com.sun.xml.internal.ws.org.objectweb.asm.Type.VOID;
import static org.objectweb.asm.Opcodes.T_BOOLEAN;
import static org.objectweb.asm.Opcodes.T_BYTE;
import static org.objectweb.asm.Opcodes.T_CHAR;
import static org.objectweb.asm.Opcodes.T_DOUBLE;
import static org.objectweb.asm.Opcodes.T_FLOAT;
import static org.objectweb.asm.Opcodes.T_INT;
import static org.objectweb.asm.Opcodes.T_LONG;
import static org.objectweb.asm.Opcodes.T_SHORT;

import org.objectweb.asm.Type;

public class ASMUtils {
    public static Type getTypeBySort(int sort) {
        switch(sort) {
        case T_BOOLEAN:
            return Type.BOOLEAN_TYPE; //Type.getType("[Z");
        case T_CHAR:
            return Type.CHAR_TYPE; //Type.getType("[C");
        case T_FLOAT:
            return Type.FLOAT_TYPE; //Type.getType("[F");
        case T_DOUBLE:
            return Type.DOUBLE_TYPE; //Type.getType("[D");
        case T_BYTE:
            return Type.BYTE_TYPE; //Type.getType("[B");
        case T_SHORT:
            return Type.SHORT_TYPE; //Type.getType("[S");
        case T_INT:
            return Type.INT_TYPE; //Type.getType("[I");
        case T_LONG:
            return Type.LONG_TYPE; //Type.getType("[J");
        }
        throw new RuntimeException("invalid primitive type code " + sort);
    }
    
    public static Type getTypeByBinaryName(String binaryName) {
        if (binaryName.startsWith("[")) {
            return Type.getType(binaryName);
        }
        return Type.getType("L" + binaryName + ";");
    }
    
    public static Type getTypeByDescriptor(String descriptor) {
        return Type.getType(descriptor);
    }

    public static Type getComponentType(Type type) {
        if (type.getSort() != Type.ARRAY) {
            throw new IllegalArgumentException("Only array has component type");
        }

        if (type.getDimensions() == 1) {
            return type.getElementType();
        }

        String descriptor = type.getDescriptor();
        return getTypeByDescriptor(descriptor.substring(1));
    }

    public static String getBinaryName(Type type) {
        return type.getInternalName();
    }

    private static Type ObjectType = ASMUtils.getTypeByBinaryName("java/lang/Object");
    private static Type SerializableType = ASMUtils.getTypeByBinaryName("java/io/Serializable");
    private static Type CloneableType = ASMUtils.getTypeByBinaryName("java/lang/Cloneable");
    
    public static Type getObjectType() {
        return ObjectType;
    }
    
    public static Type getSerializableType() {
        return SerializableType;
    }
    
    public static Type getCloneableType() {
        return CloneableType;
    }
    

    public static Class<?> getPrimitiveClass(Type type) {
        switch (type.getSort()) {
        case BYTE:
            return byte.class;
        case BOOLEAN:
            return boolean.class;
        case CHAR:
            return char.class;
        case SHORT:
            return short.class;
        case INT:
            return int.class;
        case FLOAT:
            return float.class;
        case LONG:
            return long.class;
        case DOUBLE:
            return double.class;
        case VOID:
            return void.class;
        }
        throw new RuntimeException("Unknown sort of primitive type: " + type);
    }

    public static Type getTypeByClass(Class<?> cls) {
        return Type.getType(cls);
    }

}
