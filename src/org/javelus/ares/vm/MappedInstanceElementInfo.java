package org.javelus.ares.vm;

public class MappedInstanceElementInfo extends InstanceElementInfo {

    
    Object object;
    Heap heap;
    public MappedInstanceElementInfo(Heap heap, ClassInfo classInfo, Object object) {
        super(classInfo);
        this.heap = heap;
        this.object = object;
    }
    
    protected Value getDefaultValue(FieldInfo fi) {
        return heap.loadField(fi, object);
    }

}
