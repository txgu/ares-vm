package org.javelus.ares.vm;

public class AresVMInternalException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 8153979734789505402L;

    public AresVMInternalException() {
        super();
    }

    public AresVMInternalException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public AresVMInternalException(String message, Throwable cause) {
        super(message, cause);
    }

    public AresVMInternalException(String message) {
        super(message);
    }

    public AresVMInternalException(Throwable cause) {
        super(cause);
    }

    
}
