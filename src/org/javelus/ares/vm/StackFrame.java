package org.javelus.ares.vm;

import org.objectweb.asm.Type;

public class StackFrame {
    private StackFrame previous;

    private int numOfLocals;
    private int numOfStack;

    private Value[] values;

    private int depth;
    private int top;

    private Instruction pc;

    private MethodInfo methodInfo;

    public StackFrame(MethodInfo mi) {
        this(mi.getMaxLocals(), mi.getMaxStack());
        this.pc = mi.getFirstInstruction();
        this.methodInfo = mi;
    }

    public StackFrame(int maxLocals, int maxStack) {
        this.numOfLocals = maxLocals;
        this.numOfStack = maxStack;
        this.values = createLocalsAndExpressionStack(numOfLocals + numOfStack);
        this.top = numOfLocals - 1;
    }

    protected Value[] createLocalsAndExpressionStack(int length) {
        return new Value[length];
    }

    public void setPrevious(StackFrame previous) {
        this.previous = previous;
        if (previous != null) {
            this.depth = previous.depth + 1;
        }
    }

    public int setLocal(int argOffset, Value value) {
        values[argOffset] = value;
        return argOffset + 1;
    }

    public int setLongLocal(int argOffset, Value value) {
        values[argOffset] = value;
        return argOffset + 2;
    }

    public Value getLocal(int argOffset) {
        return values[argOffset];
    }

    public Value getLongLocal(int argOffset) {
        return values[argOffset];
    }

    public ClassInfo resolveClassInfo(Type type) {
        return methodInfo.getClassInfo().getClassLoaderInfo().getResolvedClassInfo(type);
    }

    public ClassInfo resolveClassInfoOrNull(Type type) {
        return methodInfo.getClassInfo().getClassLoaderInfo().getResolvedClassInfoOrNull(type);
    }

    public MethodInfo resolveMethodVirtual(ElementInfo receiver, String name, String descriptor) {
        return receiver.getClassInfo().getMethod(name, descriptor);
    }

    public MethodInfo resolveMethod(String owner, String name, String descriptor) {
        Type ownerType = ASMUtils.getTypeByBinaryName(owner);
        ClassInfo target = resolveClassInfoOrNull(ownerType);
        if (target == null) {
            return null;
        }
        return resolveMethod(target, name, descriptor);
    }

    public MethodInfo resolveMethod(ClassInfo ci, String name, String descriptor) {
        return ci.getMethod(name, descriptor);
    }

    public FieldInfo resolveField(String owner, String name, String descriptor) {
        Type ownerType = ASMUtils.getTypeByBinaryName(owner);
        ClassInfo target = resolveClassInfoOrNull(ownerType);
        if (target == null) {
            return null;
        }
        return target.getField(name);
    }

    public StackFrame getPrevious() {
        return previous;
    }

    public void push(Value value) {
        top++;
        values[top] = value;
    }

    public void pushLong(Value value) {
        top++;
        values[top] = value;
        top++;
    }

    public Value pop() {
        Value ret = values[top];
        values[top] = null;
        top--;
        return ret;
    }

    public Value popLong() {
        values[top] = null;
        top--;
        return pop();
    }

    public Value peek() {
        return peek(0);
    }

    public Value peekLong() {
        return peek(1);
    }

    public Value peek(int i) {
        return values[top - i];
    }

    public Instruction getPC() {
        return pc;
    }

    public void setPC(Instruction pc) {
        this.pc = pc;
    }

    public void advance() {
        pc = pc.getNext();
    }



    /**
     * v -> v, v
     */
     public void dup() {
         push(peek());
     }

     /**
      * ..., v2, v1 -> ..., v1, v2, v1
      */
     public void dup_x1() {
         Value v1 = pop();
         Value v2 = pop();
         push(v1);
         push(v2);
         push(v1);
     }

     /**
      * ..., v3, v2, v1 -> ..., v1, v3, v2, v1
      */
     public void dup_x2() {
         Value v1 = pop();
         Value v2 = pop();
         Value v3 = pop();
         push(v1);
         push(v3);
         push(v2);
         push(v1);
     }

     /**
      * ..., v2, v1 -> ..., v2, v1, v2, v1
      */
     public void dup2() {
         Value v1 = pop();
         Value v2 = pop();
         push(v2);
         push(v1);
         push(v2);
         push(v1);
     }

     /**
      * ..., v3, v2, v1 -> ..., v2, v1, v3, v2, v1
      */
     public void dup2_x1() {
         Value v1 = pop();
         Value v2 = pop();
         Value v3 = pop();
         push(v2);
         push(v1);
         push(v3);
         push(v2);
         push(v1);
     }

     /**
      * ..., v4, v3, v2, v1 -> ..., v2, v1, v3, v2, v1
      */
     public void dup2_x2() {
         Value v1 = pop();
         Value v2 = pop();
         Value v3 = pop();
         Value v4 = pop();
         push(v2);
         push(v1);
         push(v4);
         push(v3);
         push(v2);
         push(v1);
     }

     /**
      * ..., v2, v1 -> ..., v1, v2
      */
     public void swap() {
         Value v1 = pop();
         Value v2 = pop();
         push(v1);
         push(v2);
     }

     public MethodInfo getMethodInfo() {
         return methodInfo;
     }

     public void pop(int size) {
         for (int i=0; i<size; i++) {
             pop();
         }
     }

     public String toString() {
         StringBuilder sb = new StringBuilder();
         sb.append(methodInfo.toString());
         sb.append(", depth=");
         sb.append(depth);
         sb.append(", values=[");
         for (int i = 0; i <= top; i++) {
             if (i == numOfLocals) {
                 sb.append('|');
             } else if (i > 0) {
                 sb.append(',');
             }
             sb.append(values[i]);
         }
         sb.append(']');
         return sb.toString();
     }

     public void clearExpressionStack() {
         int base = numOfLocals - 1;
         for (int i=base; i<=top; i++) {
             values[i] = null;
         }
         top = base;
     }

     public void setupArguments(int size) {
         System.arraycopy(previous.values, previous.top - size + 1, values, 0, size);
     }
     
     public void pushMethodArgs(Value[] args) {
         int argOffset = 0;
         int argIndex = 0;
         if (!methodInfo.isStatic()) {
             argOffset = setLocal(argOffset, args[argIndex++]);
         }

         Type[] types = methodInfo.getArgumentTypes();
         for (int i=0; i<types.length; i++) {
             int sort = types[i].getSort();
             switch (sort) {
             case Type.DOUBLE:
             case Type.LONG:
                 argOffset = setLongLocal(argOffset, args[argIndex++]);
                 break;
             case Type.FLOAT:
             case Type.INT:
             case Type.CHAR:
             case Type.BYTE:
             case Type.BOOLEAN:
             case Type.SHORT:
             default:
                 argOffset = setLocal(argOffset, args[argIndex++]);
             }
         }
     }

}
