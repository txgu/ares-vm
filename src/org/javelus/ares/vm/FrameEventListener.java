package org.javelus.ares.vm;

public interface FrameEventListener {
    /**
     * After the frame has been pushed
     * @param ti
     */
    void framePush(ThreadInfo ti);

    /**
     * Before the frame has been popped
     * @param ti
     */
    void framePop(ThreadInfo ti);
}
