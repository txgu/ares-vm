package org.javelus.ares.vm;

public class ObjectValue implements Value {
    Object object;

    public ObjectValue(Object object) {
        this.object = object;
    }
    
    public Object getObject() {
        return object;
    }

}
