package org.javelus.ares.vm;

import java.util.Map;

public abstract class HostSandboxMapping<H, S> {
    
    protected abstract Map<H, S> createHostToSandboxMap();
    
    protected abstract Map<S, H> createSandboxToHostMap();
    
    private Map<H, S> hostToSandbox;
    private Map<S, H> sandboxToHost;
    
    public HostSandboxMapping() {
        this.hostToSandbox = createHostToSandboxMap();
        this.sandboxToHost = createSandboxToHostMap();
    }
    
    public void addMapping(H h, S s) {
        if (this.hostToSandbox.containsKey(h)) {
            throw new IllegalArgumentException("Duplicate mapping");
        }
        if (this.sandboxToHost.containsKey(s)) {
            throw new IllegalArgumentException("Duplicate mapping");
        }
        this.hostToSandbox.put(h, s);
        this.sandboxToHost.put(s, h);
    }
    
    public boolean hasHost(H h) {
        return this.hostToSandbox.containsKey(h);
    }
    
    public boolean hasSandbox(S s) {
        return this.sandboxToHost.containsKey(s);
    }
    
    public S getSandbox(H h) {
        return this.hostToSandbox.get(h);
    }
    
    public H getHost(S s) {
        return this.sandboxToHost.get(s);
    }
}
