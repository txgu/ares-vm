package org.javelus.ares.vm;

import static org.objectweb.asm.Opcodes.*;

import org.javelus.ares.vm.value.AddressValue;
import org.javelus.ares.vm.value.DoubleValue;
import org.javelus.ares.vm.value.FloatValue;
import org.javelus.ares.vm.value.IntegerValue;
import org.javelus.ares.vm.value.LongValue;


public abstract class AbstractInterpreter {

    private static boolean debug = false;
    protected ThreadInfo threadInfo;

    protected Value returnValue;
    
    public AbstractInterpreter() {
        this.threadInfo = new ThreadInfo();
    }
    
    public void setReturnValue(Value ret) {
        this.returnValue = ret;
    }

    public Value getReturnValue() {
        return this.returnValue;
    }
    
    protected abstract boolean stepGuard(long step);

    protected abstract void exit(StackFrame frame);

    public void interpret() {
        StackFrame frame = null;
        long count = 0;
        while ((frame = threadInfo.getTopFrame())!= null) {
            count ++ ;
            if (stepGuard(count)) {
                System.out.println("We have executed too many instructions " + count + ", and we have to interrupt the execution");
                exit(frame);
                return;
            }
            Instruction insn = frame.getPC();
            int opcode = insn.getOpcode();
            if (opcode > 0 && debug) System.out.println("Executing " + insn + " in " + frame);
            switch(opcode) {
            case -1: // LableNode, Frame
            case NOP:
                frame.advance();
                break;
            case ACONST_NULL:
                frame.push(Value.NULL);
                frame.advance();
                break;
            case ICONST_M1:
                frame.push(IntegerValue.m1);
                frame.advance();
                break;
            case ICONST_0:
                frame.push(IntegerValue.zero);
                frame.advance();
                break;
            case ICONST_1:
                frame.push(IntegerValue.one);
                frame.advance();
                break;
            case ICONST_2:
                frame.push(IntegerValue.two);
                frame.advance();
                break;
            case ICONST_3:
                frame.push(IntegerValue.three);
                frame.advance();
                break;
            case ICONST_4:
                frame.push(IntegerValue.four);
                frame.advance();
                break;
            case ICONST_5:
                frame.push(IntegerValue.five);
                frame.advance();
                break;
            case LCONST_0:
                frame.pushLong(LongValue.zero);
                frame.advance();
                break;
            case LCONST_1:
                frame.pushLong(LongValue.one);
                frame.advance();
                break;
            case FCONST_0:
                frame.push(FloatValue.zero);
                frame.advance();
                break;
            case FCONST_1:
                frame.push(FloatValue.one);
                frame.advance();
                break;
            case FCONST_2:
                frame.push(FloatValue.two);
                frame.advance();
                break;
            case DCONST_0:
                frame.pushLong(DoubleValue.zero);
                frame.advance();
                break;
            case DCONST_1:
                frame.pushLong(DoubleValue.one);
                frame.advance();
                break;
            case IALOAD:
                arrayLoad(frame, false);
                frame.advance();
                break;
            case LALOAD:
                arrayLoad(frame, true);
                frame.advance();
                break;
            case FALOAD:
                arrayLoad(frame, false);
                frame.advance();
                break;
            case DALOAD:
                arrayLoad(frame, true);
                frame.advance();
                break;
            case AALOAD:
                arrayLoad(frame, false);
                frame.advance();
                break;
            case BALOAD:
                arrayLoad(frame, false);
                frame.advance();
                break;
            case CALOAD:
                arrayLoad(frame, false);
                frame.advance();
                break;
            case SALOAD:
                arrayLoad(frame, false);
                frame.advance();
                break;
            case IASTORE:
                arrayStore(frame, false);
                frame.advance();
                break;
            case LASTORE:
                arrayStore(frame, true);
                frame.advance();
                break;
            case FASTORE:
                arrayStore(frame, false);
                frame.advance();
                break;
            case DASTORE:
                arrayStore(frame, true);
                frame.advance();
                break;
            case AASTORE:
                arrayStore(frame, false);
                frame.advance();
                break;
            case BASTORE:
                arrayStore(frame, false);
                frame.advance();
                break;
            case CASTORE:
                arrayStore(frame, false);
                frame.advance();
                break;
            case SASTORE:
                arrayStore(frame, false);
                frame.advance();
                break;
            case POP:
                frame.pop();
                frame.advance();
                break;
            case POP2:
                frame.popLong();
                frame.advance();
                break;
            case DUP:
                frame.dup();
                frame.advance();
                break;
            case DUP_X1:
                frame.dup_x1();
                frame.advance();
                break;
            case DUP_X2:
                frame.dup_x2();
                frame.advance();
                break;
            case DUP2:
                frame.dup2();
                frame.advance();
                break;
            case DUP2_X1:
                frame.dup2_x1();
                frame.advance();
                break;
            case DUP2_X2:
                frame.dup2_x2();
                frame.advance();
                break;
            case SWAP:
                frame.swap();
                frame.advance();
                break;
            case IADD:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LADD:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case FADD:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case DADD:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case ISUB:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LSUB:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case FSUB:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case DSUB:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case IMUL:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LMUL:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case FMUL:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case DMUL:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case IDIV:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LDIV:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case FDIV:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case DDIV:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case IREM:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LREM:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case FREM:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case DREM:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case INEG:
                unaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LNEG:
                unaryOperator(frame, insn, true);
                frame.advance();
                break;
            case FNEG:
                unaryOperator(frame, insn, false);
                frame.advance();
                break;
            case DNEG:
                unaryOperator(frame, insn, true);
                frame.advance();
                break;
            case ISHL:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LSHL:
                binaryOperator(frame, insn, true, false, true);
                frame.advance();
                break;
            case ISHR:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LSHR:
                binaryOperator(frame, insn, true, false, true);
                frame.advance();
                break;
            case IUSHR:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LUSHR:
                binaryOperator(frame, insn, true, false, true);
                frame.advance();
                break;
            case IAND:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LAND:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case IOR:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LOR:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case IXOR:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LXOR:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case I2L:
                unaryOperator(frame, insn, false, true);
                frame.advance();
                break;
            case I2F:
                unaryOperator(frame, insn, false);
                frame.advance();
                break;
            case I2D:
                unaryOperator(frame, insn, false, true);
                frame.advance();
                break;
            case L2I:
                unaryOperator(frame, insn, true, false);
                frame.advance();
                break;
            case L2F:
                unaryOperator(frame, insn, true, false);
                frame.advance();
                break;
            case L2D:
                unaryOperator(frame, insn, true);
                frame.advance();
                break;
            case F2I:
                unaryOperator(frame, insn, false);
                frame.advance();
                break;
            case F2L:
                unaryOperator(frame, insn, false, true);
                frame.advance();
                break;
            case F2D:
                unaryOperator(frame, insn, false, true);
                frame.advance();
                break;
            case D2I:
                unaryOperator(frame, insn, true, false);
                frame.advance();
                break;
            case D2L:
                unaryOperator(frame, insn, true);
                frame.advance();
                break;
            case D2F:
                unaryOperator(frame, insn, true, false);
                frame.advance();
                break;
            case I2B:
                unaryOperator(frame, insn, false);
                frame.advance();
                break;
            case I2C:
                unaryOperator(frame, insn, false);
                frame.advance();
                break;
            case I2S:
                unaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LCMP:
                binaryOperator(frame, insn, true, true, false);
                frame.advance();
                break;
            case FCMPL:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case FCMPG:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case DCMPL:
                binaryOperator(frame, insn, true, true, false);
                frame.advance();
                break;
            case DCMPG:
                binaryOperator(frame, insn, true, true, false);
                frame.advance();
                break;
            case IRETURN:
                _return(frame, 1);
                break;
            case LRETURN:
                _return(frame, 2);
                break;
            case FRETURN:
                _return(frame, 1);
                break;
            case DRETURN:
                _return(frame, 2);
                break;
            case ARETURN:
                _return(frame, 1);
                break;
            case RETURN:
                _return(frame, 0);
                break;
            case ARRAYLENGTH:
                arrayLength(frame, insn);
                frame.advance();
                break;
            case ATHROW:
                _throw(frame);
                break;
            case MONITORENTER:
                frame.pop();
                frame.advance();
                break;
            case MONITOREXIT:
                frame.pop();
                frame.advance();
                break;
            case ILOAD:
                frame.push(frame.getLocal(insn.getVar()));
                frame.advance();
                break;
            case LLOAD:
                frame.pushLong(frame.getLongLocal(insn.getVar()));
                frame.advance();
                break;
            case FLOAD:
                frame.push(frame.getLocal(insn.getVar()));
                frame.advance();
                break;
            case DLOAD:
                frame.pushLong(frame.getLongLocal(insn.getVar()));
                frame.advance();
                break;
            case ALOAD:
                frame.push(frame.getLocal(insn.getVar()));
                frame.advance();
                break;
            case ISTORE:
                frame.setLocal(insn.getVar(), frame.pop());
                frame.advance();
                break;
            case LSTORE:
                frame.setLongLocal(insn.getVar(), frame.popLong());
                frame.advance();
                break;
            case FSTORE:
                frame.setLocal(insn.getVar(), frame.pop());
                frame.advance();
                break;
            case DSTORE:
                frame.setLongLocal(insn.getVar(), frame.popLong());
                frame.advance();
                break;
            case ASTORE:
                frame.setLocal(insn.getVar(), frame.pop());
                frame.advance();
                break;
            case IINC:
                iinc(frame, insn);
                frame.advance();
                break;
            case IFEQ:
            case IFNE:
            case IFLT:
            case IFGE:
            case IFGT:
            case IFLE:
                _if(frame, insn, IntegerValue.zero);
                break;
            case IF_ICMPEQ:
            case IF_ICMPNE:
            case IF_ICMPLT:
            case IF_ICMPGE:
            case IF_ICMPGT:
            case IF_ICMPLE:
            case IF_ACMPEQ:
            case IF_ACMPNE:
                _if(frame, insn, frame.pop());
                break;
            case IFNULL:
            case IFNONNULL:
                _if(frame, insn, Value.NULL);
                break;
            case GOTO:
                frame.setPC(insn.getTarget());
                break;
            case BIPUSH:
            case SIPUSH:
                frame.push(IntegerValue.valueOf(insn.getInt()));
                frame.advance();
                break;
            case NEWARRAY:
                //frame.push(newarray(frame, insn))
                newarray(frame, insn);
                frame.advance();
                break;
            case NEW:
                //frame.push(_new(frame, insn))
                _new(frame, insn);
                frame.advance();
                break;
            case ANEWARRAY:
                //frame.push(anewarray(frame, insn))
                anewarray(frame, insn);
                frame.advance();
                break;
            case CHECKCAST:
                checkcast(frame, insn);
                frame.advance();
                break;
            case INSTANCEOF:
                _instanceof(frame, insn);
                frame.advance();
                break;
            case GETSTATIC:
                getfield_or_static(frame, insn, true);
                frame.advance();
                break;
            case PUTSTATIC:
                putfield_or_static(frame, insn, true);
                frame.advance();
                break;
            case GETFIELD:
                getfield_or_static(frame, insn, false);
                frame.advance();
                break;
            case PUTFIELD:
                putfield_or_static(frame, insn, false);
                frame.advance();
                break;
            case INVOKEVIRTUAL:
                invoke(frame, insn, true, false);
                break;
            case INVOKESPECIAL:
                invoke(frame, insn, false, false);
                break;
            case INVOKESTATIC:
                invoke(frame, insn, false, true);
                break;
            case INVOKEINTERFACE:
                invoke(frame, insn, true, false);
                break;
            case INVOKEDYNAMIC:
                invokeDynamic(frame, insn);
                break;
            case LDC:
                ldc(frame, insn);
                frame.advance();
                break;
            case LOOKUPSWITCH:
                lookupswitch(frame, insn);
                break;
            case TABLESWITCH:
                tableswitch(frame, insn);
                break;
            case MULTIANEWARRAY:
                multianewarray(frame, insn);
                frame.advance();
                break;
            case RET:
                AddressValue saved = (AddressValue) frame.getLocal(insn.getVar());
                frame.setPC((Instruction)saved.getAddress());
                break;
            case JSR:
                AddressValue save = new AddressValue(frame.getPC());
                frame.push(save);
                frame.setPC(insn.getTarget());
                break;
            default:
                throw new RuntimeException("Should not reach here: " + opcode);
            }
        }

    }

    protected abstract void _if(StackFrame frame, Instruction insn, Value cond);
    protected abstract void _instanceof(StackFrame frame, Instruction insn);
    protected abstract void _new(StackFrame frame, Instruction insn);
    protected abstract void _return(StackFrame frame, int returnSize);
    protected abstract void _throw(StackFrame frame);
    protected abstract void anewarray(StackFrame frame, Instruction insn);
    protected abstract void arrayLength(StackFrame frame, Instruction insn);
    protected abstract void arrayLoad(StackFrame frame, boolean longOrDouble);
    protected abstract void arrayStore(StackFrame frame, boolean longOrDouble);
    protected abstract void binaryOperator(StackFrame frame, Instruction insn, boolean longOrDouble);
    protected abstract void binaryOperator(StackFrame frame, Instruction insn, boolean longOrDoubleOperandLeft, boolean longOrDoubleOperandRight, boolean longOrDoubleResult);
    protected abstract void checkcast(StackFrame frame, Instruction insn);
    protected abstract void getfield_or_static(StackFrame frame, Instruction insn, boolean isStatic);
    protected abstract void iinc(StackFrame frame, Instruction insn);
    protected abstract void invoke(StackFrame frame, Instruction insn, boolean isDynamic, boolean isStatic);
    protected abstract void invokeDynamic(StackFrame frame, Instruction insn);
    protected abstract void ldc(StackFrame frame, Instruction insn);
    protected abstract void lookupswitch(StackFrame frame, Instruction insn);
    protected abstract void multianewarray(StackFrame frame, Instruction insn);
    protected abstract void newarray(StackFrame frame, Instruction insn);
    protected abstract void putfield_or_static(StackFrame frame, Instruction insn, boolean isStatic);
    protected abstract void tableswitch(StackFrame frame, Instruction insn);
    protected abstract void unaryOperator(StackFrame frame, Instruction insn, boolean longOrDoubleOperand);
    protected abstract void unaryOperator(StackFrame frame, Instruction insn, boolean longOrDoubleOperand, boolean longOrDoubleResult);
}
