package org.javelus.ares.vm;

import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Map;

public class BaseMapping<H,S> extends HostSandboxMapping<H,S> {

    @Override
    protected Map<H, S> createHostToSandboxMap() {
        return new IdentityHashMap<>();
    }

    @Override
    protected Map<S, H> createSandboxToHostMap() {
        return new HashMap<>();
    }

}
