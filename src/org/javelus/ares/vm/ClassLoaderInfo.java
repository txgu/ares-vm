package org.javelus.ares.vm;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.ClassNode;

public class ClassLoaderInfo {

    /**
     * This class loader must implemented a proper getResource
     */
    ClassLoader delegator;

    Map<Type, ClassInfo> classes = new HashMap<Type, ClassInfo>();

    Metaspace metaspace;

    ClassInfo objectType;
    List<ClassInfo> arrayInterfacesTypes;

    public ClassLoaderInfo(Metaspace metaspace, ClassLoader classLoader) {
        this.metaspace = metaspace;
        this.delegator = classLoader;
    }
    
    ClassLoader getDelegator() {
        if (this.delegator == null) {
            return ClassLoader.getSystemClassLoader();
        }
        return this.delegator;
    }

    private ClassInfo findClassInfo(Type key) {
        return classes.get(key);
    }

    public ClassInfo getObjectType() {
        if (objectType == null) {
            this.objectType = getResolvedClassInfo(ASMUtils.getObjectType());
        }
        return objectType;
    }

    public List<ClassInfo> getArrayInterfaces() {
        if (this.arrayInterfacesTypes == null) {
            arrayInterfacesTypes = Arrays.asList(getResolvedClassInfo(ASMUtils.getCloneableType()),
                    getResolvedClassInfo(ASMUtils.getSerializableType()));
        }

        return this.arrayInterfacesTypes;
    }

    protected void addClassInfo(Type key, Class<?> cls, ClassInfo classInfo) {
        if (classes.put(key, classInfo) != null) {
            throw new RuntimeException("sanity check failed");
        }

        if (cls.getClassLoader() == this.delegator) {
            metaspace.mapClass(cls, classInfo);
            classInfo.setClassLoaderInfo(this);
        }
    }

    public ClassInfo getResolvedClassInfoByBinaryName(String binaryName) {
        Type type = ASMUtils.getTypeByBinaryName(binaryName);
        return getResolvedClassInfo(type);
    }

    public ClassInfo getResolvedClassInfo(Type type) {
        ClassInfo ci = getResolvedClassInfoOrNull(type);
        if (ci == null) {
            throw new RuntimeException("Cannot resolve class " + type);
        }
        return ci;
    }

    public ClassInfo getResolvedClassInfoOrNullByBinaryName(String binaryName) {
        Type type = ASMUtils.getTypeByBinaryName(binaryName);
        return getResolvedClassInfoOrNull(type);
    }

    public ClassInfo getResolvedClassInfoOrNull(Type type) {
        return resolveClassInfo(type);
    }

    private ClassInfo resolveClassInfo(Type type) {
        ClassInfo ret = findClassInfo(type);
        if (ret != null) {
            return ret;
        }

        Class<?> hostClass = loadHostClass(type);
        return resolveClassInfo(type, hostClass);
    }

    private ClassInfo resolveClassInfo(Type type, Class<?> hostClass) {
        ClassInfo ret = findClassInfo(type);
        if (ret != null) {
            return ret;
        }

        if (hostClass == null) {
            throw new RuntimeException("Not implemented");
        }

        ClassLoader defineLoader = hostClass.getClassLoader();
        if (defineLoader != this.delegator) {
            ClassLoaderInfo defineLoaderInfo = metaspace.getSandboxClassLoader(hostClass.getClassLoader());
            ret = defineLoaderInfo.resolveClassInfo(type);
            if (ret == null) {
                throw new RuntimeException("Not implemented");
            }
            addClassInfo(type, hostClass, ret);
            return ret;
        }

        if (type.getSort() == Type.ARRAY) {
            return resolveArray(type, hostClass);
        }

        if (type.getSort() != Type.OBJECT) {
            return resolvePrimitive(type, hostClass);
        }

        return resolveClass(type, hostClass);

    }

    protected URL getResource(String fileName) {
        URL file = getDelegator().getResource(fileName);
        return file;
    }

    protected Class<?> loadHostClass(Type type) {
        if (type.getSort() == Type.OBJECT) {
            try {
                return getDelegator().loadClass(type.getClassName());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            return null;
        }

        if (type.getSort() == Type.ARRAY) {
            try {
                return Class.forName(type.getDescriptor());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            return null;
        }

        return ASMUtils.getPrimitiveClass(type);
    } 

    private ClassInfo resolveClass(Type type, Class<?> cls) {
        ClassInfo ret = findClassInfo(type);
        if (ret != null) {
            return ret;
        }

        if (cls.getClassLoader() != this.delegator) {
            throw new RuntimeException("Sanity check failed");
        }

        String binaryName = ASMUtils.getBinaryName(type);

        String fileName = binaryName + ".class";
        URL file = getResource(fileName);
        if (file != null) {
            ret = loadClassInfo(file);
        }

        if (ret == null) {
            return null;
        }

        if (!ret.isLinked()) {
            throw new RuntimeException("Cannot link class " + binaryName);
        }

        addClassInfo(type, cls, ret);
        return ret;
    }

    private ClassInfo resolvePrimitive(Type type, Class<?> cls) {
        ClassInfo ci = findClassInfo(type);
        if (ci != null) {
            return ci;
        }

        if (cls.getClassLoader() != this.delegator) {
            throw new RuntimeException("sanity check failed");
        }
        ci = new ClassInfo(type);
        addClassInfo(type, cls, ci);
        return ci;
    }

    private ClassInfo resolveArray(Type arrayType, Class<?> cls) {
        ClassInfo ci = findClassInfo(arrayType);
        if (ci != null) {
            return ci;
        }

        if (cls.getClassLoader() != this.delegator) {
            throw new RuntimeException("Sanity check failed");
        }

        Class<?> compCls = cls.getComponentType();
        Type compType = ASMUtils.getComponentType(arrayType);
        ClassInfo compCi = resolveClassInfo(compType, compCls);
        
        ci = new ClassInfo(compCi, getObjectType(), getArrayInterfaces(), arrayType);
        
        addClassInfo(arrayType, cls, ci);
        return ci;
    }

    private ClassInfo loadClassInfo(URL file) {
        InputStream stream = null;

        try {
            stream = file.openStream();
            ClassReader cr = new ClassReader(stream);
            ClassNode cn = new ClassNode();
            cr.accept(cn, 0);
            ClassInfo theClass = null;

            if (cn.superName == null) {
                if (cn.name.equals("java/lang/Object")) {
                    theClass = new ClassInfo(null, Collections.<ClassInfo>emptyList(), cn);
                    return theClass;
                }
                throw new RuntimeException("Malformed class file");
            }

            ClassInfo superClass = getResolvedClassInfoOrNull(ASMUtils.getTypeByBinaryName(cn.superName));
            if (superClass == null) {
                return null;
            }

            List<ClassInfo> itfcClasses = new ArrayList<ClassInfo>();
            for(String itfcName : (List<String>)cn.interfaces) {
                ClassInfo itfcClass = getResolvedClassInfoOrNull(ASMUtils.getTypeByBinaryName(itfcName));
                if (itfcClass == null) {
                    return null;
                }
                itfcClasses.add(itfcClass);
            }
            theClass = new ClassInfo(superClass, itfcClasses, cn);
            return theClass;
        } catch (IOException e) {
            throw new RuntimeException("Cannot load class at " + file, e);
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                }
            }
        }
    }

    public static String getClassName(Type type) {
        int sort = type.getSort();
        switch (sort) {
        case Type.BOOLEAN:
            return "boolean";
        case Type.CHAR:
            return "char";
        case Type.BYTE:
            return "byte";
        case Type.SHORT:
            return "short";
        case Type.INT:
            return "int";
        case Type.FLOAT:
            return "float";
        case Type.LONG:
            return "long";
        case Type.DOUBLE:
            return "double";
        case Type.ARRAY:
        case Type.OBJECT:
            return type.getInternalName();
        default:
            throw new RuntimeException("Should not reach here");
        }
    }

}
