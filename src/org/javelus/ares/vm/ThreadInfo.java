package org.javelus.ares.vm;

import java.util.ArrayList;
import java.util.List;

public class ThreadInfo {

    StackFrame top;

    private ElementInfo pendingException;

    private List<FrameEventListener> frameListeners = new ArrayList<FrameEventListener>();

    public ElementInfo getPendingException() {
        return pendingException;
    }

    public StackFrame getTopFrame() {
        return top;
    }


    public void setPendingException(ElementInfo pendingException) {
        this.pendingException = pendingException;
    }

    public StackFrame popFrame() {
        notifyFramePop();
        StackFrame callee = top;
        top = top.getPrevious();
        callee.setPrevious(null);
        return top;
    }

    public void pushFrame(StackFrame frame) {
        frame.setPrevious(top);
        top = frame;
        notifyFramePush();
    }

    public void handleException(StackFrame frame, ClassInfo ci) {
        while(frame != null) {
            MethodInfo mi = frame.getMethodInfo();
            Instruction handler = null;
            try {
                handler = mi.exceptionHandler(frame, ci);
            } catch (RuntimeException e) {
                while (frame != null) {
                    frame = popFrame();
                }
                return;
            }
            if (handler != null) {
                frame.clearExpressionStack();
                frame.push(pendingException); 
                frame.setPC(handler);
                clearPendingException();
                return;
            }
            frame = popFrame();
        }
    }



    public void addFrameListener(FrameEventListener listener) {
        frameListeners.add(listener);
    }

    public void removeFrameListener(FrameEventListener listener) {
        frameListeners.remove(listener);
    }
    /**
     * After the frame has been pushed
     * @param ti
     */
    void notifyFramePush() {
//        for (FrameEventListener l:frameListeners) {
//            l.framePush(this);
//        }
    }

    /**
     * Before the frame has been popped
     * @param ti
     */
    void notifyFramePop() {
//        for (FrameEventListener l:frameListeners) {
//            l.framePop(this);
//        }
    }

    public void clearPendingException() {
        this.pendingException = null;
    }

    public void printStackTrace() {
        if (top == null) {
            System.out.println("Empty stack");
        }
        for (StackFrame frame = top; frame != null; frame = frame.getPrevious()) {
            System.out.format("    %s.%s @ %s\n", frame.getMethodInfo().getClassName(), frame.getMethodInfo().getFullName(), frame.getPC());
        }
    }

}
