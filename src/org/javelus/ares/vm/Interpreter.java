package org.javelus.ares.vm;

import static org.objectweb.asm.Opcodes.D2F;
import static org.objectweb.asm.Opcodes.D2I;
import static org.objectweb.asm.Opcodes.D2L;
import static org.objectweb.asm.Opcodes.DADD;
import static org.objectweb.asm.Opcodes.DCMPG;
import static org.objectweb.asm.Opcodes.DCMPL;
import static org.objectweb.asm.Opcodes.DDIV;
import static org.objectweb.asm.Opcodes.DMUL;
import static org.objectweb.asm.Opcodes.DNEG;
import static org.objectweb.asm.Opcodes.DREM;
import static org.objectweb.asm.Opcodes.DSUB;
import static org.objectweb.asm.Opcodes.F2D;
import static org.objectweb.asm.Opcodes.F2I;
import static org.objectweb.asm.Opcodes.F2L;
import static org.objectweb.asm.Opcodes.FADD;
import static org.objectweb.asm.Opcodes.FCMPG;
import static org.objectweb.asm.Opcodes.FCMPL;
import static org.objectweb.asm.Opcodes.FDIV;
import static org.objectweb.asm.Opcodes.FMUL;
import static org.objectweb.asm.Opcodes.FNEG;
import static org.objectweb.asm.Opcodes.FREM;
import static org.objectweb.asm.Opcodes.FSUB;
import static org.objectweb.asm.Opcodes.I2B;
import static org.objectweb.asm.Opcodes.I2C;
import static org.objectweb.asm.Opcodes.I2D;
import static org.objectweb.asm.Opcodes.I2F;
import static org.objectweb.asm.Opcodes.I2L;
import static org.objectweb.asm.Opcodes.I2S;
import static org.objectweb.asm.Opcodes.IADD;
import static org.objectweb.asm.Opcodes.IAND;
import static org.objectweb.asm.Opcodes.IDIV;
import static org.objectweb.asm.Opcodes.IFEQ;
import static org.objectweb.asm.Opcodes.IFGE;
import static org.objectweb.asm.Opcodes.IFGT;
import static org.objectweb.asm.Opcodes.IFLE;
import static org.objectweb.asm.Opcodes.IFLT;
import static org.objectweb.asm.Opcodes.IFNE;
import static org.objectweb.asm.Opcodes.IFNONNULL;
import static org.objectweb.asm.Opcodes.IFNULL;
import static org.objectweb.asm.Opcodes.IF_ACMPEQ;
import static org.objectweb.asm.Opcodes.IF_ACMPNE;
import static org.objectweb.asm.Opcodes.IF_ICMPEQ;
import static org.objectweb.asm.Opcodes.IF_ICMPGE;
import static org.objectweb.asm.Opcodes.IF_ICMPGT;
import static org.objectweb.asm.Opcodes.IF_ICMPLE;
import static org.objectweb.asm.Opcodes.IF_ICMPLT;
import static org.objectweb.asm.Opcodes.IF_ICMPNE;
import static org.objectweb.asm.Opcodes.IINC;
import static org.objectweb.asm.Opcodes.IMUL;
import static org.objectweb.asm.Opcodes.INEG;
import static org.objectweb.asm.Opcodes.IOR;
import static org.objectweb.asm.Opcodes.IREM;
import static org.objectweb.asm.Opcodes.ISHL;
import static org.objectweb.asm.Opcodes.ISHR;
import static org.objectweb.asm.Opcodes.ISUB;
import static org.objectweb.asm.Opcodes.IUSHR;
import static org.objectweb.asm.Opcodes.IXOR;
import static org.objectweb.asm.Opcodes.L2D;
import static org.objectweb.asm.Opcodes.L2F;
import static org.objectweb.asm.Opcodes.L2I;
import static org.objectweb.asm.Opcodes.LADD;
import static org.objectweb.asm.Opcodes.LAND;
import static org.objectweb.asm.Opcodes.LCMP;
import static org.objectweb.asm.Opcodes.LDIV;
import static org.objectweb.asm.Opcodes.LMUL;
import static org.objectweb.asm.Opcodes.LNEG;
import static org.objectweb.asm.Opcodes.LOR;
import static org.objectweb.asm.Opcodes.LREM;
import static org.objectweb.asm.Opcodes.LSHL;
import static org.objectweb.asm.Opcodes.LSHR;
import static org.objectweb.asm.Opcodes.LSUB;
import static org.objectweb.asm.Opcodes.LUSHR;
import static org.objectweb.asm.Opcodes.LXOR;

import java.util.List;

import org.javelus.ares.vm.value.BooleanValue;
import org.javelus.ares.vm.value.ByteValue;
import org.javelus.ares.vm.value.CharValue;
import org.javelus.ares.vm.value.ClassInfoValue;
import org.javelus.ares.vm.value.DoubleValue;
import org.javelus.ares.vm.value.FloatValue;
import org.javelus.ares.vm.value.IntegerValue;
import org.javelus.ares.vm.value.LongValue;
import org.javelus.ares.vm.value.ShortValue;
import org.javelus.ares.vm.value.StringValue;
import org.objectweb.asm.Type;


public class Interpreter extends AbstractInterpreter {

    private long maximumStep = 1000L;

    Heap heap;
    Metaspace metaspace;

    public Interpreter(Metaspace metaspace) {
        this.metaspace = metaspace;
        this.heap = new Heap(metaspace);
    }

    public Heap getHeap() {
        return heap;
    }
    
    public Metaspace getMetaspace() {
        return this.metaspace;
    }
    
    @Override
    protected boolean stepGuard(long step) {
        if (step > this.maximumStep) {
            return true;
        }
        return false;
    }

    @Override
    protected void exit(StackFrame frame) {
        exitWithException(frame, "java/lang/InterruptedException");
    }

    private void exitWithException(StackFrame frame, String className) {
        Type type = ASMUtils.getTypeByBinaryName(className);
        ClassInfo ci = frame.resolveClassInfo(type);
        ElementInfo exception = newException(ci);
        threadInfo.setPendingException(exception);
        if (frame != threadInfo.getTopFrame()) {
            throw new RuntimeException("sanity check failed");
        }
        while (frame != null) {
            frame = threadInfo.popFrame();
        }
    }

    @Override
    protected void _if(StackFrame frame, Instruction insn, Value v2) {
        Value v1 = frame.pop();
        boolean result = evaluateIf(insn, v1, v2);
        if (result) {
            frame.setPC(insn.getTarget());
        } else {
            frame.setPC(insn.getNext());
        }

    }

    private boolean evaluateIf(Instruction insn, Value v1, Value v2) {
        int opcode = insn.getOpcode();
        switch (opcode) {
        case IFEQ: {
            int i = getIntValue(v1);
            return i == 0; }
        case IFNE: {
            int i = getIntValue(v1);
            return i != 0; }
        case IFLT: {
            int i = getIntValue(v1);
            return i < 0; }
        case IFGE: {
            int i = getIntValue(v1);
            return i >= 0; }
        case IFGT: {
            int i = getIntValue(v1);
            return i > 0;}
        case IFLE: {
            int i = getIntValue(v1);
            return i <= 0; }
        case IF_ICMPEQ: {
            int i1 = getIntValue(v1);
            int i2 = getIntValue(v2);
            return i1 == i2; }
        case IF_ICMPNE: {
            int i1 = getIntValue(v1);
            int i2 = getIntValue(v2);
            return i1 != i2 ; }
        case IF_ICMPLT: {
            int i1 = getIntValue(v1);
            int i2 = getIntValue(v2);
            return i1 < i2; }
        case IF_ICMPGE: {
            int i1 = getIntValue(v1);
            int i2 = getIntValue(v2);
            return i1 >= i2; }
        case IF_ICMPGT: {
            int i1 = getIntValue(v1);
            int i2 = getIntValue(v2);
            return i1 > i2; }
        case IF_ICMPLE: {
            int i1 = getIntValue(v1);
            int i2 = getIntValue(v2);
            return i1 <= i2; }
        case IF_ACMPEQ: {
            ElementInfo e1 = getElementInfo(v1);
            ElementInfo e2 = getElementInfo(v2);
            return e1 == e2; }
        case IF_ACMPNE: {
            ElementInfo e1 = getElementInfo(v1);
            ElementInfo e2 = getElementInfo(v2);
            return e1 != e2; }
        case IFNULL:
            if (getElementInfo(v1) == null) {
                return true;
            } else {
                return false;
            }
        case IFNONNULL:
            if (getElementInfo(v1) != null) {
                return true;
            } else {
                return false;
            }
        }
        throw new IllegalStateException("Should not reach here");
    }

    public static int getIntValue(Value v) {
        if (v instanceof IntegerValue) {
            return ((IntegerValue) v).getValue();
        } else if (v instanceof ByteValue) {
            return ((ByteValue)v).getValue();
        } else if (v instanceof BooleanValue) {
            return ((BooleanValue)v).getValue() ? 1 : 0;
        } else if (v instanceof CharValue) {
            return ((CharValue)v).getValue();
        }

        throw new RuntimeException("Unknown type: " + v);
    }

    @Override
    protected void _instanceof(StackFrame frame, Instruction insn) {
        Value object = frame.peek();
        Type type = ASMUtils.getTypeByBinaryName(insn.getDesc());
        ClassInfo target = frame.resolveClassInfoOrNull(type);
        if (target == null) {
            createAndThrowClassNotFoundException(frame);
            return;
        }
        if (object == Value.NULL) {
            frame.pop();
            frame.push(IntegerValue.zero);
            return;
        }

        ElementInfo ei = getElementInfo(object);
        frame.pop();
        if (ei != null) {
            ClassInfo eici = ei.getClassInfo();
            if (eici.isInstanceOf(target)) {
                frame.push(IntegerValue.one);
            } else {
                frame.push(IntegerValue.zero);
            }
        } else {
            throw new NullPointerException();
        }
    }

    @Override
    protected void _new(StackFrame frame, Instruction insn) {
        Type type = ASMUtils.getTypeByBinaryName(insn.getDesc());
        ClassInfo target = frame.resolveClassInfoOrNull(type);
        ElementInfo object = heap.newInstance(target);
        frame.push(object);
    }

    @Override
    protected void _return(StackFrame frame, int returnSize) {
        Value ret = null;
        if (returnSize == 1) {
            ret = frame.peek(); // do not pop, we may use this value during framePop event
        } else if (returnSize == 2) {
            ret = frame.peekLong();
        }

        MethodInfo mi = frame.getMethodInfo();
        StackFrame caller = threadInfo.popFrame();

        if (caller == null) {
            setReturnValue(ret);
            return;
        }

        caller.pop(mi.getArgumentsSize());

        if (returnSize == 1) {
            caller.push(ret);
        } else if (returnSize == 2) {
            caller.pushLong(ret);
        }

        caller.advance();
    }

    protected ElementInfo newException(ClassInfo ci) {
        return new InstanceElementInfo(ci);
    }

    protected void createAndThrowException(StackFrame frame, String className) {
        Type type = ASMUtils.getTypeByBinaryName(className);
        ClassInfo ci = frame.resolveClassInfo(type);
        ElementInfo exception = newException(ci);
        threadInfo.setPendingException(exception);
        threadInfo.handleException(frame, ci);
    }

    protected void createAndThrowNullPointerException(StackFrame frame) {
        createAndThrowException(frame, "java/lang/NullPointerException");
    }

    protected void createAndThrowClassNotFoundException(StackFrame frame) {
        createAndThrowException(frame, "java/lang/ClassNotFoundException");
    }


    @Override
    protected void _throw(StackFrame frame) {
        Value exception = frame.pop();
        ElementInfo ei = getElementInfo(exception);
        if (ei == null) {
            createAndThrowNullPointerException(frame);
            return;
        }
        threadInfo.setPendingException(ei);
        ClassInfo ci = ei.getClassInfo();
        threadInfo.handleException(frame, ci);
    }

    @Override
    protected void anewarray(StackFrame frame, Instruction insn) {
        Type type = ASMUtils.getTypeByBinaryName(insn.getDesc());
        ElementInfo ei = newarrayCommon(frame, insn, type, frame.pop());
        frame.push(ei);
    }

    protected ElementInfo getElementInfo(Value v) {
        if (v == null) {
            return null;
        }

        if (v instanceof ElementInfo) {
            return ((ElementInfo) v);
        }

        throw new RuntimeException("Unknown type: " + v);
    }

    @Override
    protected void arrayLength(StackFrame frame, Instruction insn) {
        Value array = frame.peek();
        if (array == Value.NULL) {
            createAndThrowNullPointerException(frame);
            return;
        }

        ElementInfo ei = getElementInfo(array);
        if (ei == null) {
            throw new NullPointerException();
        }
        frame.pop();
        frame.push(ei.arrayLength());
    }

    @Override
    protected void arrayLoad(StackFrame frame, boolean longOrDouble) {
        Value array = frame.peek(1);
        if (array == Value.NULL) {
            createAndThrowNullPointerException(frame);
            return;
        }
        ElementInfo ei = getElementInfo(array);
        Value index = frame.pop();
        frame.pop();

        Value value;
        if (ei != null) {
            value = ei.getElement(getIntValue(index));
        } else {
            throw new NullPointerException();
        }

        if (longOrDouble) {
            frame.pushLong(value);
        } else {
            frame.push(value);
        }

    }

    @Override
    protected void arrayStore(StackFrame frame, boolean longOrDouble) {
        Value array = frame.peek(longOrDouble?3:2);

        if (array == Value.NULL) {
            createAndThrowNullPointerException(frame);
            return;
        }

        ElementInfo ei = getElementInfo(array);

        Value value = null;
        if (longOrDouble) {
            value = frame.popLong();
        } else {
            value = frame.pop();
        }
        Value index = frame.pop();
        array = frame.pop();

        if (ei != null) {
            ei.setElement(getIntValue(index), value);
        } else {
            throw new NullPointerException();
        }
    }

    @Override
    protected void binaryOperator(StackFrame frame, Instruction insn, boolean longOrDoubleOperand) {
        binaryOperator(frame, insn, longOrDoubleOperand, longOrDoubleOperand, longOrDoubleOperand);
    }

    @Override
    protected void binaryOperator(StackFrame frame, Instruction insn, boolean longOrDoubleOperandLeft,
            boolean longOrDoubleOperandRight, boolean longOrDoubleResult) {

        Value v1, v2, result = null;
        if (longOrDoubleOperandRight) {
            v2 = frame.popLong();
        } else {
            v2 = frame.pop();
        }

        if (longOrDoubleOperandLeft) {
            v1 = frame.popLong();
        } else {
            v1 = frame.pop();
        }

        try {
            result = evaluateExpr(insn, v1, v2);
        } catch (java.lang.ArithmeticException e) {
            createAndThrowException(frame, "java.lang.ArithmeticException");
            return;
        }
        if (longOrDoubleResult) {
            frame.pushLong(result);
        } else {
            frame.push(result);
        }

    }

    private Value evaluateExpr(Instruction insn, Value v1, Value v2) {
        int opcode = insn.getOpcode();

        switch (opcode) {
        case IINC:
        case IADD: {
            int c1 = getIntValue(v1);
            int c2 = getIntValue(v2);
            return IntegerValue.valueOf(c1 + c2); }
        case LADD: {
            LongValue c1 = (LongValue) v1;
            LongValue c2 = (LongValue) v2;
            return LongValue.valueOf(c1.getValue() + c2.getValue()); }
        case FADD: {
            FloatValue c1 = (FloatValue) v1;
            FloatValue c2 = (FloatValue) v2;
            return FloatValue.valueOf(c1.getValue() + c2.getValue()); }
        case DADD: {
            DoubleValue c1 = (DoubleValue) v1;
            DoubleValue c2 = (DoubleValue) v2;
            return DoubleValue.valueOf(c1.getValue() + c2.getValue()); }
        case ISUB: {
            int c1 = getIntValue(v1);
            int c2 = getIntValue(v2);
            return IntegerValue.valueOf(c1 - c2); }
        case LSUB: {
            LongValue c1 = (LongValue) v1;
            LongValue c2 = (LongValue) v2;
            return LongValue.valueOf(c1.getValue() - c2.getValue()); }
        case FSUB: {
            FloatValue c1 = (FloatValue) v1;
            FloatValue c2 = (FloatValue) v2;
            return FloatValue.valueOf(c1.getValue() - c2.getValue()); }
        case DSUB: {
            DoubleValue c1 = (DoubleValue) v1;
            DoubleValue c2 = (DoubleValue) v2;
            return DoubleValue.valueOf(c1.getValue() - c2.getValue()); }
        case IMUL: {
            int c1 = getIntValue(v1);
            int c2 = getIntValue(v2);
            return IntegerValue.valueOf(c1 * c2); }
        case LMUL: {
            LongValue c1 = (LongValue) v1;
            LongValue c2 = (LongValue) v2;
            return LongValue.valueOf(c1.getValue() * c2.getValue()); }
        case FMUL: {
            FloatValue c1 = (FloatValue) v1;
            FloatValue c2 = (FloatValue) v2;
            return FloatValue.valueOf(c1.getValue() * c2.getValue()); }
        case DMUL: {
            DoubleValue c1 = (DoubleValue) v1;
            DoubleValue c2 = (DoubleValue) v2;
            return DoubleValue.valueOf(c1.getValue() - c2.getValue()); }
        case IDIV: {
            int c1 = getIntValue(v1);
            int c2 = getIntValue(v2);
            return IntegerValue.valueOf(c1 / c2); }
        case LDIV: {
            LongValue c1 = (LongValue) v1;
            LongValue c2 = (LongValue) v2;
            return LongValue.valueOf(c1.getValue() / c2.getValue()); }
        case FDIV: {
            FloatValue c1 = (FloatValue) v1;
            FloatValue c2 = (FloatValue) v2;
            return FloatValue.valueOf(c1.getValue() / c2.getValue()); }
        case DDIV: {
            DoubleValue c1 = (DoubleValue) v1;
            DoubleValue c2 = (DoubleValue) v2;
            return DoubleValue.valueOf(c1.getValue() / c2.getValue()); }
        case IREM: {
            int c1 = getIntValue(v1);
            int c2 = getIntValue(v2);
            return IntegerValue.valueOf(c1 % c2); }
        case LREM: {
            LongValue c1 = (LongValue) v1;
            LongValue c2 = (LongValue) v2;
            return LongValue.valueOf(c1.getValue() % c2.getValue()); }
        case FREM: {
            FloatValue c1 = (FloatValue) v1;
            FloatValue c2 = (FloatValue) v2;
            return FloatValue.valueOf(c1.getValue() % c2.getValue()); }
        case DREM: {
            DoubleValue c1 = (DoubleValue) v1;
            DoubleValue c2 = (DoubleValue) v2;
            return DoubleValue.valueOf(c1.getValue() % c2.getValue()); }
        case INEG: {
            int c1 = getIntValue(v1);
            return IntegerValue.valueOf(-c1); }
        case LNEG: {
            LongValue c1 = (LongValue) v1;
            return LongValue.valueOf(-c1.getValue()); }
        case FNEG: {
            FloatValue c1 = (FloatValue) v1;
            return FloatValue.valueOf(-c1.getValue()); }
        case DNEG: {
            DoubleValue c1 = (DoubleValue) v1;
            return DoubleValue.valueOf(-c1.getValue()); }
        case ISHL: {
            int c1 = getIntValue(v1);
            int c2 = getIntValue(v2);
            return IntegerValue.valueOf(c1 << c2); }
        case LSHL: {
            LongValue c1 = (LongValue) v1;
            int c2 = getIntValue(v2);
            return LongValue.valueOf(c1.getValue() << c2); }
        case ISHR: {
            int c1 = getIntValue(v1);
            int c2 = getIntValue(v2);
            return IntegerValue.valueOf(c1 >> c2); }
        case LSHR: {
            LongValue c1 = (LongValue) v1;
            int c2 = getIntValue(v2);
            return LongValue.valueOf(c1.getValue() >> c2); }
        case IUSHR: {
            int c1 = getIntValue(v1);
            int c2 = getIntValue(v2);
            return IntegerValue.valueOf(c1 >>> c2); }
        case LUSHR: {
            LongValue c1 = (LongValue) v1;
            int c2 = getIntValue(v2);
            return LongValue.valueOf(c1.getValue() >>> c2); }
        case IAND: {
            int c1 = getIntValue(v1);
            int c2 = getIntValue(v2);
            return IntegerValue.valueOf(c1 & c2); }
        case LAND: {
            LongValue c1 = (LongValue) v1;
            LongValue c2 = (LongValue) v2;
            return LongValue.valueOf(c1.getValue() & c2.getValue()); }
        case IOR: {
            int c1 = getIntValue(v1);
            int c2 = getIntValue(v2);
            return IntegerValue.valueOf(c1 | c2); }
        case LOR: {
            LongValue c1 = (LongValue) v1;
            LongValue c2 = (LongValue) v2;
            return LongValue.valueOf(c1.getValue() | c2.getValue()); }
        case IXOR: {
            int c1 = getIntValue(v1);
            int c2 = getIntValue(v2);
            return IntegerValue.valueOf(c1 ^ c2); }
        case LXOR: {
            LongValue c1 = (LongValue) v1;
            LongValue c2 = (LongValue) v2;
            return LongValue.valueOf(c1.getValue() ^ c2.getValue()); }
        case I2L: {
            int c1 = getIntValue(v1);
            return LongValue.valueOf(c1); }
        case I2F: {
            int c1 = getIntValue(v1);
            return FloatValue.valueOf(c1); }
        case I2D: {
            int c1 = getIntValue(v1);
            return DoubleValue.valueOf(c1); }
        case L2I: {
            LongValue c1 = (LongValue) v1;
            return IntegerValue.valueOf((int)c1.getValue()); }
        case L2F: {
            LongValue c1 = (LongValue) v1;
            return FloatValue.valueOf((float)c1.getValue()); }
        case L2D: {
            LongValue c1 = (LongValue) v1;
            return DoubleValue.valueOf((double)c1.getValue()); }
        case F2I: {
            FloatValue c1 = (FloatValue) v1;
            return IntegerValue.valueOf((int)c1.getValue()); }
        case F2L: {
            FloatValue c1 = (FloatValue) v1;
            return LongValue.valueOf((long)c1.getValue()); }
        case F2D: {
            FloatValue c1 = (FloatValue) v1;
            return DoubleValue.valueOf(c1.getValue()); }
        case D2I: {
            DoubleValue c1 = (DoubleValue) v1;
            return IntegerValue.valueOf((int)c1.getValue()); }
        case D2L: {
            DoubleValue c1 = (DoubleValue) v1;
            return LongValue.valueOf((long)c1.getValue()); }
        case D2F: {
            DoubleValue c1 = (DoubleValue) v1;
            return FloatValue.valueOf((float)c1.getValue()); }
        case I2B: {
            int c1 = getIntValue(v1);
            return ByteValue.valueOf((byte)c1); }
        case I2C: {
            int c1 = getIntValue(v1);
            return CharValue.valueOf((char)c1); }
        case I2S: {
            int c1 = getIntValue(v1);
            return ShortValue.valueOf((short)c1); }
        case LCMP: {
            LongValue c1 = (LongValue) v1;
            LongValue c2 = (LongValue) v2;
            long l1 = c1.getValue();
            long l2 = c2.getValue();
            if (l1 > l2) {
                return IntegerValue.one;
            } else if (l1 == l2) {
                return IntegerValue.zero;
            } else {
                return IntegerValue.m1;
            }}
        case FCMPL: {
            FloatValue c1 = (FloatValue) v1;
            FloatValue c2 = (FloatValue) v2;
            float f1 = c1.getValue();
            float f2 = c2.getValue();
            if (Float.isNaN(f1) || Float.isNaN(f2)) {
                return IntegerValue.m1;
            }
            if (f1 > f2) {
                return IntegerValue.one;
            } else if (f1 == f2) {
                return IntegerValue.zero;
            } else {
                return IntegerValue.m1;
            }}
        case FCMPG: {
            FloatValue c1 = (FloatValue) v1;
            FloatValue c2 = (FloatValue) v2;
            float f1 = c1.getValue();
            float f2 = c2.getValue();
            if (Float.isNaN(f1) || Float.isNaN(f2)) {
                return IntegerValue.one;
            }
            if (f1 > f2) {
                return IntegerValue.one;
            } else if (f1 == f2) {
                return IntegerValue.zero;
            } else {
                return IntegerValue.m1;
            }}
        case DCMPL: {
            DoubleValue c1 = (DoubleValue) v1;
            DoubleValue c2 = (DoubleValue) v2;
            double d1 = c1.getValue();
            double d2 = c2.getValue();
            if (Double.isNaN(d1) || Double.isNaN(d2)) {
                return IntegerValue.m1;
            }
            if (d1 > d2) {
                return IntegerValue.one;
            } else if (d1 == d2) {
                return IntegerValue.zero;
            } else {
                return IntegerValue.m1;
            }}
        case DCMPG: {
            DoubleValue c1 = (DoubleValue) v1;
            DoubleValue c2 = (DoubleValue) v2;
            double d1 = c1.getValue();
            double d2 = c2.getValue();
            if (Double.isNaN(d1) || Double.isNaN(d2)) {
                return IntegerValue.one;
            }
            if (d1 > d2) {
                return IntegerValue.one;
            } else if (d1 == d2) {
                return IntegerValue.zero;
            } else {
                return IntegerValue.m1;
            }}
        }
        throw new RuntimeException("Should not reach here " + insn);
    }

    @Override
    protected void checkcast(StackFrame frame, Instruction insn) {
        Value obj = frame.peek();
        if (obj == Value.NULL) {
            return;
        }
        ElementInfo ei = getElementInfo(obj);

        if (ei != null) {
            ClassInfo eici = ei.getClassInfo();
            Type type = ASMUtils.getTypeByBinaryName(insn.getDesc());
            ClassInfo target = frame.resolveClassInfoOrNull(type);
            if (target == null) {
                createAndThrowClassNotFoundException(frame);
                return;
            }
            if (!eici.isInstanceOf(target)) {
                createAndThrowException(frame, "java/lang/ClassCastException");
                return;
            }
        } else {
            throw new NullPointerException();
        }
    }

    @Override
    protected void getfield_or_static(StackFrame frame, Instruction insn, boolean isStatic) {
        FieldInfo fi = frame.resolveField(insn.getFieldOwner(), insn.getFieldName(), insn.getFieldDesc());
        if (fi == null) {
            fi = frame.resolveField(insn.getFieldOwner(), insn.getFieldName(), insn.getFieldDesc());
            createAndThrowException(frame, "java/lang/NoSuchFieldError");
            return;
        }
        ElementInfo ei;
        Value ret;
        if (isStatic) {
            ret = heap.getStaticField(fi);
        } else {
            Value recv = frame.peek();
            if (recv == Value.NULL) {
                createAndThrowNullPointerException(frame);
                return;
            }
            ei = getElementInfo(recv);
            recv = frame.pop();
            if (ei != null) {
                ret = ei.getField(fi);
            } else {
                throw new NullPointerException();
            }
        }

        if (fi.getSize() == 1) {
            frame.push(ret);
        } else {
            frame.pushLong(ret);
        }

    }

    @Override
    protected void iinc(StackFrame frame, Instruction insn) {
        Value v = frame.getLocal(insn.getIincVar());
        Value incr = IntegerValue.valueOf(insn.getIincIncr());
        frame.setLocal(insn.getIincVar(), evaluateExpr(insn, v, incr));
    }

    @Override
    protected void invoke(StackFrame frame, Instruction insn, boolean isDynamic, boolean isStatic) {

        Value recv = null;
        String owner = insn.getMethodOwner();
        String name = insn.getMethodName();
        String desc = insn.getMethodDesc();
        MethodInfo staticCallee = frame.resolveMethod(owner, name, desc);
        if (staticCallee == null) {
            // infeasible path of instanceof
            System.err.println("Cannot find method " + owner + "." + name + desc);
            frame.resolveMethod(owner, name, desc);
            createAndThrowException(frame, "java/lang/NoSuchMethodError");
            return;
        }
        MethodInfo callee = null;
        int argumentSize = staticCallee.getArgumentsSize();
        ElementInfo ei = null;
        if (!isStatic) {
            recv = frame.peek(argumentSize-1);
            if (recv == Value.NULL) {
                createAndThrowNullPointerException(frame);
                return;
            }
            ei = getElementInfo(recv);
            if (ei == null) {
                throw new NullPointerException();
            }
        }

        if (isDynamic) {
            callee = frame.resolveMethodVirtual(ei, name, desc);
            if (callee == null) {
                // infeasible path of instanceof
                createAndThrowException(frame, "java/lang/IncompatibleClassChangeError");
                return;
            }
        } else {
            callee = staticCallee;
        }

        if (callee.isAbstract()) {
            createAndThrowException(frame, "java/lang/AbstractMethodError");
            return;
        }

        if (callee.isNative()) {
            createAndThrowException(frame, "java/lang/UnsatisfiedLinkError");
            return;
        }


        StackFrame calleeFrame = new StackFrame(callee);
        threadInfo.pushFrame(calleeFrame);
        calleeFrame.setupArguments(argumentSize);
    }

    @Override
    protected void invokeDynamic(StackFrame frame, Instruction insn) {
        System.err.println("invokedynamic is not supported");
        exit(frame);
    }

    @Override
    protected void ldc(StackFrame frame, Instruction insn) {
        Object cst = insn.getConstant();

        if (cst instanceof Integer) {
            frame.push(IntegerValue.valueOf((Integer)cst));
        } else if (cst instanceof Float) {
            frame.push(FloatValue.valueOf((Float)cst));
        } else if (cst instanceof Double) {
            frame.pushLong(DoubleValue.valueOf((Double)cst));
        } else if (cst instanceof Long) {
            frame.pushLong(LongValue.valueOf((Long)cst));
        } else if (cst instanceof String) {
            frame.push(StringValue.valueOf((String)cst));
        } else if (cst instanceof Type) {
            Type type = (Type) cst;
            ClassInfo target = frame.resolveClassInfoOrNull(type);
            if (target == null) {
                createAndThrowClassNotFoundException(frame);
                return;
            }
            frame.push(ClassInfoValue.valueOf(target));
        } else {
            throw new RuntimeException("Should not reach here");
        }
    }

    @Override
    protected void lookupswitch(StackFrame frame, Instruction insn) {
        Value key = frame.pop();
        int branch = evaluateLookupSwitch(insn, key);
        Instruction target;
        if (branch == -1) {
            target = insn.getLookupSwitchDefault();
        } else {
            target = insn.getLookupSwitchBranch(branch);
        }

        frame.setPC(target);
    }

    private int evaluateLookupSwitch(Instruction insn, Value key) {
        int ik = getIntValue(key);
        List<Integer> keys = insn.getLookupSwitchKeys();
        for (int i = 0; i < keys.size(); i++) {
            int sk = keys.get(i);
            if (ik == sk) {
                return i;
            }
        }
        return -1;
    }

    @Override
    protected void multianewarray(StackFrame frame, Instruction insn) {
        int dims = insn.getMultiDims();
        int[] arrayLengths = new int[dims];
        for (int i = dims - 1; i >= 0; i--) {
            arrayLengths[i] = getIntValue(frame.pop());
        }
        Type type = ASMUtils.getTypeByBinaryName(insn.getMultiArrayDesc());
        ClassInfo target = frame.resolveClassInfoOrNull(type);
        if (target == null) {
            createAndThrowClassNotFoundException(frame);
            return;
        }
        ElementInfo array = heap.newMultiArray(target, arrayLengths);
        frame.push(array);
    }

    @Override
    protected void newarray(StackFrame frame, Instruction insn) {
        ElementInfo ei = newarrayCommon(frame, insn, ASMUtils.getTypeBySort(insn.getInt()), frame.pop());
        frame.push(ei);
    }

    protected ElementInfo newarrayCommon(StackFrame frame, Instruction insn, Type arrayType, Value length) {
        ClassInfo target = frame.resolveClassInfoOrNull(arrayType);
        if (target == null) {
            createAndThrowClassNotFoundException(frame);
            return null;
        }
        return heap.newArray(target, getIntValue(length));
    }

    @Override
    protected void putfield_or_static(StackFrame frame, Instruction insn, boolean isStatic) {
        FieldInfo fi = frame.resolveField(insn.getFieldOwner(), insn.getFieldName(), insn.getFieldDesc());
        if (fi == null) {
            createAndThrowException(frame, "java/lang/NoSuchFieldError");
            return;
        }
        Value recv = null;
        ElementInfo ei = null;
        if (!isStatic) {
            recv = frame.peek(fi.getSize());
            if (recv == Value.NULL) {
                createAndThrowNullPointerException(frame);
                return;
            }
            ei = getElementInfo(recv);
        }

        Value value;
        if (fi.getSize() == 1) {
            value = frame.pop();
        } else {
            value = frame.popLong();
        }

        if (isStatic) {
            heap.setStaticField(fi, value);
        } else {
            frame.pop(); // recv;
            if (ei != null) {
                ei.setField(fi, value);
            } else {
                throw new NullPointerException();
            }
        }

    }

    @Override
    protected void tableswitch(StackFrame frame, Instruction insn) {
        Value index = frame.pop();
        int branch = evaluateTableSwitch(insn, index);
        Instruction target;
        if (branch == -1) {
            target = insn.getTableSwitchDefault();
        } else {
            target = insn.getTableSwitchBranch(branch);
        }

        frame.setPC(target);
    }

    private int evaluateTableSwitch(Instruction insn, Value index) {
        int max = insn.getTableSwitchMax();
        int min = insn.getTableSwitchMin();
        int i = getIntValue(index);
        if (i >= min  && i <= max) {
            return i - min;
        }
        return -1;
    }

    @Override
    protected void unaryOperator(StackFrame frame, Instruction insn, boolean longOrDoubleOperand) {
        unaryOperator(frame, insn, longOrDoubleOperand, longOrDoubleOperand);
    }

    @Override
    protected void unaryOperator(StackFrame frame, Instruction insn, boolean longOrDoubleOperand,
            boolean longOrDoubleResult) {
        Value v, result;
        if (longOrDoubleOperand) {
            v = frame.popLong();
        } else {
            v = frame.pop();
        }

        result = evaluateExpr(insn, v, null);

        if (longOrDoubleResult) {
            frame.pushLong(result);
        } else {
            frame.push(result);
        }
    }

    public long getMaximumStep() {
        return maximumStep;
    }

    public void setMaximumStep(long maximumStep) {
        this.maximumStep = maximumStep;
    }

    
    public void execute(MethodInfo methodInfo, Value[] args) {
        StackFrame frame = new StackFrame(methodInfo);
        frame.pushMethodArgs(args);
        threadInfo.pushFrame(frame);

        interpret();
    }
}
