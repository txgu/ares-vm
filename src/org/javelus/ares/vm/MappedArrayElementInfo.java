package org.javelus.ares.vm;

import java.lang.reflect.Array;

public class MappedArrayElementInfo extends ArrayElementInfo implements Cloneable {

    Object array;
    Heap heap;
    public MappedArrayElementInfo(Heap heap, ClassInfo classInfo, Object array) {
        super(classInfo, Array.getLength(array));
        this.heap = heap;
        this.array = array;
    }

    protected Value getDefaultValue(int index) {
        return heap.loadArrayElement(this, array, index);
    }
}
