package org.javelus.ares.vm;

import org.javelus.ares.vm.value.IntegerValue;

import com.sun.xml.internal.ws.org.objectweb.asm.Type;

public class ArrayElementInfo extends ElementInfo implements Cloneable {

    public ArrayElementInfo(ClassInfo classInfo, int length) {
        super(classInfo);
        this.length = new IntegerValue(length);
        this.elements = new Value[length];
    }

    public final IntegerValue length;
    public final Value[] elements;

    public ArrayElementInfo clone() {
        return null;
    }

    protected Value getDefaultValue(int index) {
        return Value.getDefaultValue(this.getClassInfo().getLowerDimensionClassInfo().getTypeDescriptor());
    }

    
    public Value arrayLength() {
        return this.length;
    }
    
    
    public Value getElement(int index) {
        Value value = elements[index];
        if (value == null) {
            value = getDefaultValue(index);
            elements[index] = value;
        }
        return value;
    }
    
    public void setElement(int index, Value value) {
        elements[index] = value; 
    }
    
    public boolean isReferenceArray() {
        int sort = getClassInfo().getType().getElementType().getSort();
        return sort == Type.ARRAY || sort == Type.OBJECT;
    }
}
