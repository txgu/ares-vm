package org.javelus.ares.vm.value;

import org.javelus.ares.vm.Value;

public class AddressValue implements Value {
    private Object address;

    public AddressValue(Object address) {
        this.address = address;
    }

    public Object getAddress() {
        return address;
    }
}
