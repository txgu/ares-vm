package org.javelus.ares.vm.value;

import java.util.HashMap;
import java.util.Map;

import org.javelus.ares.vm.Value;

public class FloatValue implements Value {


    private float value;

    public FloatValue(float value) {
        this.value = value;
    }

    public float getValue() {
        return this.value;
    }

    static Map<Float, FloatValue> values = new HashMap<Float, FloatValue>();

    public static FloatValue defaultValue = valueOf(0.0F);

    public static FloatValue zero = valueOf(0.0F);

    public static FloatValue one = valueOf(1.0F);

    public static FloatValue two = valueOf(2.0F);

    public static FloatValue valueOf(float v) {
        FloatValue value = values.get(v);
        if (value == null) {
            value = new FloatValue(v);
            values.put(v, value);
        }
        return value;
    }

    public String toString() {
        if (Float.isNaN(getValue())) {
            return "Float.NaN";
        }
        if (Float.isInfinite(getValue())) {
            return String.format("Float.intBitsToFloat(%d)", Float.floatToIntBits(getValue()));
        }
        return "(float)" + getValue() + 'F';
    }
}
