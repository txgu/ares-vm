package org.javelus.ares.vm.value;

import java.util.HashMap;
import java.util.Map;

import org.javelus.ares.vm.Value;

public class ByteValue implements Value  {
    private byte value;

    public ByteValue(byte value) {
        this.value = value;
    }

    public byte getValue() {
        return this.value;
    }

    static Map<Byte, ByteValue> values = new HashMap<Byte, ByteValue>();

    public static ByteValue defaultValue = new ByteValue((byte) 0);

    public static ByteValue valueOf(byte v) {
        ByteValue value = values.get(v);
        if (value == null) {
            value = new ByteValue(v);
            values.put(v, value);
        }
        return value;
    }

    public String toString() {
        return "(byte)" + getValue();
    }
}
