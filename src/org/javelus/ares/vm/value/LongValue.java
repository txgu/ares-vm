package org.javelus.ares.vm.value;

import java.util.HashMap;
import java.util.Map;

import org.javelus.ares.vm.Value;

public class LongValue implements Value {
    private long value;

    public LongValue(long value) {
        this.value = value;
    }

    public long getValue() {
        return this.value;
    }

    static Map<Long, LongValue> values = new HashMap<Long, LongValue>();

    public static LongValue defaultValue = valueOf(0L);

    public static LongValue zero = defaultValue;
    public static LongValue one = valueOf(1L);


    public static LongValue valueOf(long v) {
        LongValue value = values.get(v);
        if (value == null) {
            value = new LongValue(v);
            values.put(v, value);
        }
        return value;
    }

    public String toString() {
        return "(long)" + getValue() + 'L';
    }
}
