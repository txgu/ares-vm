package org.javelus.ares.vm.value;

import org.javelus.ares.vm.ClassInfo;
import org.javelus.ares.vm.Value;

public class ClassInfoValue implements Value  {
    private ClassInfo ci;

    public ClassInfoValue(ClassInfo ci) {
        this.ci = ci;
    }

    public ClassInfo getClassInfo() {
        return ci;
    }

    public static ClassInfoValue valueOf(ClassInfo ci) {
        return new ClassInfoValue(ci);
    }

    public String getTypeDescriptor() {
        return "Ljava/lang/Class;";
    }
}
