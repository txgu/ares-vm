package org.javelus.ares.vm.value;

import java.util.HashMap;
import java.util.Map;

import org.javelus.ares.vm.Value;

public class CharValue implements Value  {
    private char value;

    public CharValue(char value) {
        this.value = value;
    }

    public char getValue() {
        return this.value;
    }

    static Map<Character, CharValue> values = new HashMap<Character, CharValue>();

    public static CharValue defaultValue = new CharValue('\0');

    public static CharValue valueOf(char v) {
        CharValue value = values.get(v);
        if (value == null) {
            value = new CharValue(v);
            values.put(v, value);
        }
        return value;
    }

    public String toString() {
        return String.format("(char)'\\%d'", ((int)getValue()));
    }
}
