package org.javelus.ares.vm.value;

import org.javelus.ares.vm.Value;

public class BooleanValue implements Value  {
    private boolean value;

    private BooleanValue(boolean value) {
        this.value = value;
    }

    public boolean getValue() {
        return this.value;
    }

    public static BooleanValue TRUE = new BooleanValue(true);
    public static BooleanValue FALSE = new BooleanValue(false);
    public static BooleanValue defaultValue = FALSE;

    public String toString() {
        return "(boolean)" + getValue();
    }
}
