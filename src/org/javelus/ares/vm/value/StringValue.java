package org.javelus.ares.vm.value;

import org.javelus.ares.vm.Value;

public class StringValue implements Value {

    private String value;

    public StringValue(String value) {
        this.value = escape(value);
    }

    public String getValue() {
        return value;
    }

    public static StringValue valueOf(String s){
        return new StringValue(s);
    }

    public static String escape(String value) {
        return value.replace("\\", "\\\\").replace("\n", "\\n").replace("\"", "\\\"").replace("\r", "\\r");
    }

    public String toString() {
        return "\"" + getValue() + "\"";
    }
}
