package org.javelus.ares.vm;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.objectweb.asm.Type;



/**
 * Put all reflection and adapter here
 * 
 * @author t
 *
 */
public class Metaspace {

    BaseMapping<Class<?>, ClassInfo> classes;
    BaseMapping<ClassLoader, ClassLoaderInfo> classLoaders;

    BaseMapping<Field, FieldInfo> fields;
    BaseMapping<Method, MethodInfo> methods;
    BaseMapping<Constructor<?>, MethodInfo> constructors;


    public Metaspace() {
        this.classes = new BaseMapping<Class<?>,ClassInfo>();
        this.classLoaders = new BaseMapping<ClassLoader,ClassLoaderInfo>();
        this.fields = new BaseMapping<Field,FieldInfo>();
        this.methods = new BaseMapping<Method,MethodInfo>();
        this.constructors = new BaseMapping<Constructor<?>,MethodInfo>();
    }

    public Class<?> getHostClass(ClassInfo ci) {
        Class<?> cls = classes.getHost(ci);
        if (cls == null) {
            throw new RuntimeException("Sanity check failed");
        }
        return cls;
    }

    public Method getHostMethod(MethodInfo mi) {
        Method method = methods.getHost(mi);
        if (method != null) {
            return method;
        }
        ClassInfo ci = mi.getClassInfo();
        ClassLoaderInfo cli = ci.getClassLoaderInfo();
        Class<?> cls = getHostClass(ci);
        Type[] argTypes = mi.getArgumentTypes();
        Class<?>[] argCls = new Class<?>[argTypes.length];
        for (int i = 0; i < argTypes.length; i++) {
            Type t = argTypes[i];
            argCls[i] = cli.loadHostClass(t);
        }
        try {
            method = cls.getDeclaredMethod(mi.getName(), argCls);
            method.setAccessible(true);
            methods.addMapping(method, mi);
            return method;
        } catch (Throwable e) {
            return null;
        }
    }

    public Field getHostField(FieldInfo fi) {
        Field field = fields.getHost(fi);
        if (field != null) {
            return field;
        }
        ClassInfo ci = fi.getClassInfo();
        Class<?> cls = getHostClass(ci);
        try {
            field = cls.getDeclaredField(fi.getName());
            field.setAccessible(true);
            fields.addMapping(field, fi);
            return field;
        } catch (Throwable e) {
            return null;
        }
    }

    public ClassInfo getSandboxClass(Class<?> cls) {
        ClassInfo ci = classes.getSandbox(cls);
        if (ci == null) {
            ClassLoader cl = cls.getClassLoader();
            ClassLoaderInfo cli = getSandboxClassLoader(cl);
            return cli.getResolvedClassInfo(ASMUtils.getTypeByClass(cls));
        }

        return ci;
    }

    public ClassLoaderInfo getSandboxClassLoader(ClassLoader classLoader) {
        ClassLoaderInfo cli = classLoaders.getSandbox(classLoader);
        if (cli == null) {
            cli = new ClassLoaderInfo(this, classLoader);
            classLoaders.addMapping(classLoader, cli);
        }
        return cli;
    }

    public void mapClass(Class<?> cls, ClassInfo classInfo) {
        if (classes.hasHost(cls) || classes.hasSandbox(classInfo)) {
            throw new IllegalStateException();
        }
        this.classes.addMapping(cls, classInfo);
    }
}
