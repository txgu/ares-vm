package org.javelus.ares.vm;

public abstract class ElementInfo implements Cloneable, Value{

    private ClassInfo classInfo;

    public ElementInfo(ClassInfo classInfo) {
        this.classInfo = classInfo;
    }

    public ClassInfo getClassInfo() {
        return classInfo;
    }

    public ElementInfo clone() {
        return null;
    }

    public String toString() {
        return "(" + classInfo.getSimpleName() + ")";
    }
    
    public Value arrayLength() {
        throw new RuntimeException();
    }
    
    public Value getElement(int index) {
        throw new RuntimeException();
    }
    
    public void setElement(int index, Value element) {
        throw new RuntimeException();
    }
    
    public Value getField(FieldInfo field) {
        throw new RuntimeException();
    }
    
    public void setField(FieldInfo field, Value value) {
        throw new RuntimeException();
    }
}
