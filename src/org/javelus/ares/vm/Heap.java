package org.javelus.ares.vm;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.javelus.ares.vm.value.BooleanValue;
import org.javelus.ares.vm.value.ByteValue;
import org.javelus.ares.vm.value.CharValue;
import org.javelus.ares.vm.value.DoubleValue;
import org.javelus.ares.vm.value.FloatValue;
import org.javelus.ares.vm.value.IntegerValue;
import org.javelus.ares.vm.value.LongValue;
import org.javelus.ares.vm.value.ShortValue;
import org.objectweb.asm.Type;

public class Heap {
    Metaspace metaspace;

    BaseMapping<Object, ElementInfo> objects = new BaseMapping<Object, ElementInfo>();

    Map<FieldInfo, Value> staticFields = new HashMap<FieldInfo, Value>();

    public Heap(Metaspace metaspace) {
        this.metaspace = metaspace;
    }

    public void setStaticField(FieldInfo field, Value value) {
        if (!field.isStatic()) {
            throw new IllegalArgumentException();
        }
        if (value == null) {
            throw new NullPointerException();
        }
        staticFields.put(field, value);
    }


    public Value getStaticField(FieldInfo field) {
        if (!field.isStatic()) {
            throw new IllegalArgumentException();
        }
        Value value = staticFields.get(field);
        if (value == null) {
            value = loadField(field, null);
            staticFields.put(field, value);
        }
        return value;
    }
    
    Value loadField(FieldInfo fi, Object object) {
        Field field = getMetaspace().getHostField(fi);
        Type type = fi.getType();
        int sort = type.getSort();

        try {
            if (sort == Type.ARRAY || sort == Type.OBJECT) {
                return getMappedObject(field.get(object));
            }
            switch (sort) {
            case Type.BYTE:
                return new ByteValue(field.getByte(object));
            case Type.CHAR:
                return new CharValue(field.getChar(object));
            case Type.BOOLEAN:
                return (field.getBoolean(object)) ? BooleanValue.TRUE : BooleanValue.FALSE;
            case Type.SHORT:
                return new ShortValue(field.getShort(object));
            case Type.INT:
                return new IntegerValue(field.getInt(object));
            case Type.FLOAT:
                return new FloatValue(field.getFloat(object));
            case Type.DOUBLE:
                return new DoubleValue(field.getDouble(object));
            case Type.LONG:
                return new LongValue(field.getLong(object));
            }
        } catch (Throwable e) {
            throw new AresVMInternalException(e);
        }
        return null;
    
    }

    public ElementInfo newInstance(ClassInfo target) {
        return new InstanceElementInfo(target);
    }

    private ElementInfo newMappedInstance(ClassInfo target, Object origin) {
        return new MappedInstanceElementInfo(this, target, origin);
    }
    
    public ElementInfo newArray(ClassInfo target, int length) {
        return new ArrayElementInfo(target, length);
    }
    
    private ElementInfo newMappedArray(ClassInfo target, Object origin) {
        return new MappedArrayElementInfo(this, target, origin);
    }

    public ElementInfo newMultiArray(ClassInfo target, int[] arrayLengths) {
        return newMultiArray(target, arrayLengths, 0);
    }

    private ElementInfo newMultiArray(ClassInfo target, int[] arrayLengths, int i) {
        int length = arrayLengths[i];
        ElementInfo array = new ArrayElementInfo(target, length);
        if (i < arrayLengths.length - 1) {
            for (int j = 0; j < length; j++) {
                array.setElement(j, newMultiArray(target.getLowerDimensionClassInfo(), arrayLengths, i+1));
            }
        }
        return array;
    }
    
    Value loadArrayElement(ElementInfo ei, Object array, int index) {
        Type type = ei.getClassInfo().getLowerDimensionClassInfo().getType();
        int sort = type.getSort();
        if (sort == Type.ARRAY || sort == Type.OBJECT) {
            return new ObjectValue(Array.get(array, index));
        }
        switch (sort) {
        case Type.BYTE:
            return new ByteValue(Array.getByte(array, index));
        case Type.CHAR:
            return new CharValue(Array.getChar(array, index));
        case Type.BOOLEAN:
            return Array.getBoolean(array, index) ? BooleanValue.TRUE : BooleanValue.FALSE;
        case Type.SHORT:
            return new ShortValue(Array.getShort(array, index));
        case Type.INT:
            return new IntegerValue(Array.getInt(array, index));
        case Type.FLOAT:
            return new FloatValue(Array.getFloat(array, index));
        case Type.DOUBLE:
            return new DoubleValue(Array.getDouble(array, index));
        case Type.LONG:
            return new LongValue(Array.getLong(array, index));
        }
        throw new IllegalStateException("Unknonwn type: " + type);
    }

    public Value getMappedObject(Object object) {
        if (object == null) {
            return Value.NULL;
        }
        Class<?> cls = object.getClass();
        ClassInfo ci = metaspace.getSandboxClass(cls);
        if (cls.isArray()) {
            return newMappedArray(ci, object);
        }
        return newMappedInstance(ci, object);
    }

    public Metaspace getMetaspace() {
        return metaspace;
    }
}
