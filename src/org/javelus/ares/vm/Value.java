package org.javelus.ares.vm;

import org.javelus.ares.vm.value.BooleanValue;
import org.javelus.ares.vm.value.ByteValue;
import org.javelus.ares.vm.value.CharValue;
import org.javelus.ares.vm.value.DoubleValue;
import org.javelus.ares.vm.value.FloatValue;
import org.javelus.ares.vm.value.IntegerValue;
import org.javelus.ares.vm.value.LongValue;
import org.javelus.ares.vm.value.ShortValue;

public interface Value {
    public static final Value NULL = new Value() {

        public String toString() {
            return "NULL";
        }

    };

    public static Value getDefaultValue(String typeDescriptor) {
        Value v = null;
        if (typeDescriptor.equals("I")) {
            v = IntegerValue.defaultValue;
        } else if(typeDescriptor.equals("C")) {
            v = CharValue.defaultValue;
        } else if(typeDescriptor.equals("S")) {
            v = ShortValue.defaultValue;
        } else if(typeDescriptor.equals("Z")) {
            v = BooleanValue.defaultValue;
        } else if(typeDescriptor.equals("B")) {
            v = ByteValue.defaultValue;
        } else if(typeDescriptor.equals("J")) {
            v = LongValue.defaultValue;
        } else if (typeDescriptor.equals("D")) {
            v = DoubleValue.defaultValue;
        } else if (typeDescriptor.equals("F")) {
            v = FloatValue.defaultValue;
        } else if (typeDescriptor.equals("V")) {
            throw new RuntimeException("Sanity check failed");
        } else {
            v = Value.NULL;
        }

        return v;
    }
}
