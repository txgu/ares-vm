import org.javelus.ares.vm.ClassInfo;
import org.javelus.ares.vm.ClassLoaderInfo;
import org.javelus.ares.vm.ElementInfo;
import org.javelus.ares.vm.Heap;
import org.javelus.ares.vm.Interpreter;
import org.javelus.ares.vm.Metaspace;
import org.javelus.ares.vm.MethodInfo;
import org.javelus.ares.vm.Value;

public class SimpleTest {
    static class Counter {
        int value;
        public Counter() {
            this(0);
        }
        public Counter(int value) {
            this.value = value;
        }
        int set(int value) {
            this.value = value;
            return this.value;
        }
        int increment() {
            this.value++;
            int [][] newArrays = new int[10][2];
            for (int i = 0; i < newArrays.length; i++) {
                newArrays[i] = new int[10];
            }
            int[] arrays = new int[10];
            for (int i=0; i<arrays.length; i++) {
                arrays[i] = value;
            }
            return this.value;
        }
    }
    
    public static void main(String[] args) {
        Metaspace ms = new Metaspace();
        Interpreter interpreter = new Interpreter(ms);
        Heap heap = interpreter.getHeap();
        ClassLoaderInfo cli = ms.getSandboxClassLoader(Counter.class.getClassLoader());
        ClassInfo counterClass = cli.getResolvedClassInfoByBinaryName("SimpleTest$Counter"); //ms.getSandboxClass(Counter.class);
        ElementInfo counter = heap.newInstance(counterClass);
    
        MethodInfo mi = counterClass.getMethod("increment", "()I");
        
        interpreter.execute(mi, new Value[]{counter});
        
        System.out.println(interpreter.getReturnValue());
    }
}
